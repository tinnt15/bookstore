import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Resolve } from '@angular/router/src/interfaces';

@Injectable()
export class BooksService {
  baseUrl: string = 'http://localhost:49490/Books/'
  constructor(
    private _http :Http) { }
  
  getBooks(){
    return this._http.get(this.baseUrl+'GetBooks')
    .map((response:Response) => response.json())
    .catch(this._errorHandler)
  }

  getBooksById(id){
    return this._http.get(this.baseUrl +"GetBooksById/"+id)
          .map((response:Response)=>response.json())
          .catch(this._errorHandler)
  }

  saveBooks(books){
    return this._http.post(this.baseUrl +   'SaveBooks', books)
            .map((response: Response) => response.json())
            .catch(this._errorHandler)
  }

  deleteBooks(id){
    return this._http.delete(this.baseUrl + "DeleteBooks/" + id)
              .map((response:Response) =>  response.json())
              .catch(this._errorHandler)
  }

  _errorHandler(error:Response){
    debugger;
    console.log(error);
    return Observable.throw(error || "Internal server error");
  }

}
